'use strict';

var ecApp = angular.module("ecApp", ['ngRoute']);

ecApp.config(function ($routeProvider, $locationProvider) {
   $routeProvider
       .when('/', {
         templateUrl: '/html/main.html',
         controller: 'ecMainController',
         resolve: {
           // I will cause a 1 second delay
           // delay: function($q, $timeout) {
           //   var delay = $q.defer();
           //   $timeout(delay.resolve, 1000);
           //   return delay.promise;
         }
       })
       .when('/precos', {
         templateUrl: '/html/precos.html',
         controller: 'ecPrecosController',
       })
       .when('/pedido', {
         templateUrl: '/html/pedido.html',
         controller: 'ecPrecosController',
       })

       //remove hash from relative links
       $locationProvider.hashPrefix('');
});